import os
import sys
import yaml
import pytest

@pytest.fixture(scope='session')
def env(request):
    config_path = os.path.join(request.config.rootdir,
                                'config',
                                request.config.getoption("environment"),
                                'config.yaml')
    with open(config_path, 'r') as f:
        env = yaml.load(f.read(), Loader=yaml.SafeLoader)
    return env

def pytest_addoption(parser):
    parser.addoption('--env',
                    dest='environment',
                    default='test',
                    help='select environment')
