import yaml

def get_test_data(test_data_path):
    case = list()
    http = list()
    expected = list()
    with open(test_data_path) as f:
        data = yaml.load(f.read(), Loader=yaml.SafeLoader)
        for ts in data['test']:
            case.append(ts.get('case', ''))
            http.append(ts.get('http', {}))
            expected.append((ts.get('expected', {})))
    params = zip(case, http, expected)
    return case, params
