import os
import sys
import pytest
import requests
from utils.lib import get_test_data

cases, params = get_test_data('data/test_in_theaters.yaml')

class TestInTheathers(object):
    @pytest.mark.parametrize('case, http, expected', list(params), ids=cases)
    def test_in_theathers(self, env, case, http, expected):
        r = requests.request(http['method'], url=env['host']['OMDb'], params=http['params']).json()
        data = r['Search'][0]
        assert data['Title'] == expected['response']['title']
        assert data['Year'] == expected['response']['year']
